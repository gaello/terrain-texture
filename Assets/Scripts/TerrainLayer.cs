﻿using UnityEngine;

/// <summary>
/// Terrain layer.
/// </summary>
[System.Serializable]
public class TerrainLayer
{
    // Terrain minimum elevation.
    public float minHeight;

    // Terrain color.
    public Color terrainColor;
}
