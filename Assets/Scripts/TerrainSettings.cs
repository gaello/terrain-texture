﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Terrain settings.
/// </summary>
[System.Serializable]
public class TerrainSettings
{
    // Different terrain types defined as layers.
    [SerializeField]
    private List<TerrainLayer> terrainLayers = new List<TerrainLayer>();

    /// <summary>
    /// Evaluates the color for the terrain.
    /// </summary>
    /// <returns>The terrain color.</returns>
    /// <param name="height">Terrain height.</param>
    public Color EvaluateColor(float height)
    {
        // Looking for the proper layer for the provided height.
        int i = terrainLayers.Count - 1;
        while (terrainLayers[i].minHeight > height)
        {
            i--;
        }
        if (i < 0) i = 0;

        // Returning terrain color.
        return terrainLayers[i].terrainColor;
    }
}
