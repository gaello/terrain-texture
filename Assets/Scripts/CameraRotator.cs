﻿using UnityEngine;

/// <summary>
/// Simple script to rotate camera.
/// </summary>
public class CameraRotator : MonoBehaviour
{
    // Camera rotation speed.
    [SerializeField]
    private float rotationSpeed = 30;

    // Camera pivot.
    [SerializeField]
    private Vector3 rotationPivot = Vector3.zero;

    /// <summary>
    /// Unity method called on every frame.
    /// </summary>
    private void Update()
    {
        transform.RotateAround(rotationPivot, Vector3.up, rotationSpeed * Time.deltaTime);
    }
}
