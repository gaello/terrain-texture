# Generating texture for the terrain mesh in Unity

So you are looking for information on how you can generate texture for the terrain mesh in Unity? That's easy! This repository contain what you looking for!

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/08/19/procedural-terrain-pt4-texture/

Enjoy!

---

# How to use it?

This repository contains an example of how you can generate texture for the terrain mesh in Unity.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/terrain-texture/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

Now you know how to generate texture for the terrain mesh in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com
